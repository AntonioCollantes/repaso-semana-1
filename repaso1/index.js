import listAlumnos from "./modulo-antonio/alumnos.js";
import {messageAproveCurso} from "./modulo-antonio/operaciones.js";
import calculaPromedio, {messagePasante} from "./modulo-antonio/operaciones.js";
import {redondearDecimales} from "./modulo-antonio/operaciones.js";

const listEvaluarAlumnos = listAlumnos.map((item) => {
  
  const numDecimalesNotas = 2; //redondear a 2 decimales
  const {alumno, seccion} = item;
  const cursosAlum = item.cursos; //Almacena informacion de cursos nombre y notas[]
  let promedioCurso, msgCurso;
  let infoCursos = []; //declaracion de arreglo para almacenar info procesada de los cursos
  let dataCurso = {}; //declaracion de objeto para guaradar info procesada de un curso
  let promedioFinal = 0;
  let msgFinal = "";

  for(let idCurso in cursosAlum) {
    promedioCurso = calculaPromedio(cursosAlum[idCurso].notas);
    promedioFinal += promedioCurso;
    msgCurso = messageAproveCurso(redondearDecimales(promedioCurso,numDecimalesNotas),cursosAlum[idCurso].nombreCurso);
    dataCurso = {
      nombreCurso: cursosAlum[idCurso].nombreCurso,
      notasCurso: cursosAlum[idCurso].notas,
      notaFinal: redondearDecimales(promedioCurso, numDecimalesNotas),
      mensaje: msgCurso
    }
    infoCursos.push(dataCurso);
  }

  promedioFinal = redondearDecimales((promedioFinal/infoCursos.length), numDecimalesNotas);
  msgFinal = messagePasante(promedioFinal);

  return {
    alumno,
    seccion,
    infoCursos,
    promedioFinal,
    msgFinal
  }
});

console.log(listEvaluarAlumnos);