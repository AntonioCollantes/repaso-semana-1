const listAlumnos = [
  {
    alumno: "Juan Collantes", 
    cursos: [
      {nombreCurso: "Matematica", notas: [20,14,18]},
      {nombreCurso: "Lenguaje", notas: [14,11,12]},
      {nombreCurso: "Hitoria", notas: [15,10,11]},
      {nombreCurso: "Trigonometria", notas: [20,18,17]},
      {nombreCurso: "Ciencias Naturales", notas: [11, 15, 19]}
    ],
    seccion: "A"
  },
  {
    alumno: "Daysi Guerra", 
    cursos: [
      {nombreCurso: "Matematica", notas: [19,20,20]},
      {nombreCurso: "Lenguaje", notas: [11,18,12]},
      {nombreCurso: "Hitoria", notas: [18,15,20]},
      {nombreCurso: "Trigonometria", notas: [20,20,17]},
      {nombreCurso: "Ciencias Naturales", notas: [10, 9, 18]}
    ],
    seccion: "A"
  },
  {
    alumno: "Susana Collantes", 
    cursos: [
      {nombreCurso: "Matematica", notas: [20,20,20]},
      {nombreCurso: "Lenguaje", notas: [19,18,19]},
      {nombreCurso: "Hitoria", notas: [20,20,18]},
      {nombreCurso: "Trigonometria", notas: [20,19,20]},
      {nombreCurso: "Ciencias Naturales", notas: [19, 19, 20]}
    ],
    seccion: "B"
  },
  {
    alumno: "Jessica Ramirez", 
    cursos: [
      {nombreCurso: "Matematica", notas: [11,10,16]},
      {nombreCurso: "Lenguaje", notas: [12,15,16]},
      {nombreCurso: "Hitoria", notas: [13,20,18]},
      {nombreCurso: "Trigonometria", notas: [10,12,9]},
      {nombreCurso: "Ciencias Naturales", notas: [11, 11, 17]}
    ],
    seccion: "C"
  },
  {
    alumno: "Jorge Ramirez", 
    cursos: [
      {nombreCurso: "Matematica", notas: [9,10,11]},
      {nombreCurso: "Lenguaje", notas: [12,5,11]},
      {nombreCurso: "Hitoria", notas: [13,11,10]},
      {nombreCurso: "Trigonometria", notas: [10,12,10]},
      {nombreCurso: "Ciencias Naturales", notas: [11, 11, 6]}
    ],
    seccion: "C"
  }
]

export default listAlumnos;