const calculaPromedio = (notas) => notas.reduce( (i, y) =>  i + y  ) / notas.length;

export const messageAproveCurso = (promedio, curso) => {
  return `Ud ${promedio < 11 ? "desaprobo" : "Aprobó"} el curso de ${curso} con un promedio de ${promedio}`;
}

export const messagePasante = (promedio) => `Su promedio General es: ${promedio}, Ud ${promedio < 11 ? "repite" : "pasa"} de año`;

export const redondearDecimales = (promedio, numDecimales) => {
  return parseFloat(promedio).toFixed(numDecimales);
}

export default calculaPromedio;